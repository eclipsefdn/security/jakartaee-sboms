# jakartaee-sboms

This repository contains a list of SBOM files generated using the [cyclonedx-maven-plugin](https://github.com/CycloneDX/cyclonedx-maven-plugin)

To query or analyse a SBOM, You can use the [sbom-utility](https://github.com/CycloneDX/sbom-utility)

Please find below an example to inspect SBOM resources type for [jakarta.platform/jakarta.jakartaee-api/10.0.0/bom.json](./jakarta.platform/jakarta.jakartaee-api/10.0.0/bom.json)
```bash
sbom-utility resource -i jakarta.platform/jakarta.jakartaee-api/10.0.0/bom.json
```

Each BOM path follows this path schema based on its Maven configuration: ${project.groupId}/${project.artifactId}/${project.version}
